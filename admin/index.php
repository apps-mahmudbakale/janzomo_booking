<?php 
session_start();
include 'lib/connection.php';
include 'lib/function.php';

 ?>
<!DOCTYPE html>
<html lang="en" >
<head>
  <meta charset="UTF-8">
  <title>Login</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel='stylesheet' href='css/bootstrap.css'>
  <link rel="stylesheet" href="style.css">

</head>
<body>
<!-- partial:index.partial.html -->
<div class="global-container">
	<div class="card login-form">
	<div class="card-body">
		<h3 class="card-title text-center">Log in</h3>
		<div class="card-text">
			<?php 
			if (isset($_POST['login'])) {
				$username = $_POST['username'];
				$password = md5($_POST['password']);

				$query = "SELECT * FROM admin WHERE username ='$username' AND password ='$password'";

				if (getRows($query) >= 1) {
					
					$row = fetchRows($query);

					$_SESSION['admin_id'] = $row['id'];
					$_SESSION['username'] = $row['username'];

					echo "<script>window.location='dashboard.php'</script>";
				}else{
					error('Wrong Details');
				}
			}

			 ?>
			<form action="" method="POST">
				<!-- to error: add class "has-danger" -->
				<div class="form-group">
					<label for="exampleInputEmail1">Username</label>
					<input type="text" name="username" class="form-control form-control-sm" id="exampleInputEmail1" aria-describedby="emailHelp">
				</div>
				<div class="form-group">
					<label for="exampleInputPassword1">Password</label>
					<a href="#" style="float:right;font-size:12px;">Forgot password?</a>
					<input type="password" name="password" class="form-control form-control-sm" id="exampleInputPassword1">
				</div>
				<button type="submit" name="login" class="btn btn-primary btn-block">Sign in</button>
			</form>
		</div>
	</div>
</div>
</div>
<!-- partial -->
  
</body>
</html>
