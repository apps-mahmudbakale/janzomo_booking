<?php session_start(); 
include 'lib/connection.php';
include 'lib/function.php';
?>
<!DOCTYPE html>
<head>
<?php require'link.php' ?>
	<title>Dashbaord</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body class="body">
<div class="container">
    <div class="col-lg-1">
    	
    </div>
    <div class="col-lg-11">
	<h2><img src="img/RedRabbit_Logo_small.png" width="100" height="100">JANZOMO FARM PRODUCTS ONLINE BOOKING SYSTEM</h2>

	</div>
<div>&nbsp;</div>
<div>
	<nav id="menubar" class= 'navbar navbar-inverse container'>
		<ul class="nav navbar-nav">
			<li><a href="dashboard.php"><i class="fa fa-home"></i> Home</a></li>
			<li><a href="products.php"><i class="fa fa-cubes"></i> Products</a></li>
			<li><a href="customers.php"><i class="fa fa-users"></i> Customers</a></li>
			<li><a href="orders.php"><i class="fa fa-random"></i> Orders</a></li>
			<li><a href="logout.php"><i class="fa fa-power-off"></i> Logout</a></li>
		</ul>
	</nav>
</div>
<div class="col-lg-12 thumbnail" style="width:103%">
	  <fieldset>
	  	   <legend><i class="fa fa-table"></i> Orders</legend>
	  	  <table class="table table-striped">
	  	  	<thead>
	  	  		<tr>
	  	  			<th>S/N</th>
	  	  			<th>Customer Name</th>
	  	  			<th>Phone</th>
	  	  			<th>Product</th>
	  	  			<th>Quantity</th>
	  	  			<th>Amount</th>
	  	  			<th>Booked On </th>
	  	  			<th>Needed On</th>
	  	  			<th></th>
	  	  		</tr>
	  	  	</thead>
	  	  	<tbody>
	  	  		<?php 
	  	  		$sn=0;
	  	  		$query = mysqli_query($db,"SELECT * FROM orders INNER JOIN products USING(product_id) INNER JOIN customer ON customer.id = orders.customer_id WHERE status ='0'");
	  	  		while ($row = mysqli_fetch_array($query)) {
	  	  			$sn++;
	  	  			echo "<tr>
	  	  				<td>".$sn."</td>
	  	  				<td>".$row[10]."</td>
	  	  				<td>".$row['phone']."</td>
	  	  				<td>".$row[7]."</td>
	  	  				<td>".$row['qty']."</td>
	  	  				<td>".number_format($row['qty'] * $row['price'])."</td>
	  	  				<td>".$row['book_on']."</td>
	  	  				<td>".$row['need_on']."</td>
	  	  				<td class='btn-group'><a class='btn btn-success btn-sm'><i class='fa fa-check-square'></i></a><a class='btn btn-danger btn-sm'><i class='fa fa-trash-o'></i></a></td>

	  	  				</tr>";
	  	  		}
	  	  		 ?>
	  	  	</tbody>
	  	  </table>
	</fieldset>

</div>
</div>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
</body>
</html>
<html>