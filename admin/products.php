<?php session_start(); 
include 'lib/connection.php';
include 'lib/function.php';
?>
<!DOCTYPE html>
<head>
<?php require'link.php' ?>
	<title>Dashbaord</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body class="body">
<div class="container">
    <div class="col-lg-1">
    	
    </div>
    <div class="col-lg-11">
	<h2><img src="img/RedRabbit_Logo_small.png" width="100" height="100">JANZOMO FARM PRODUCTS ONLINE BOOKING SYSTEM</h2>

	</div>
<div>&nbsp;</div>
<div>
	<nav id="menubar" class= 'navbar navbar-inverse container'>
		<ul class="nav navbar-nav">
			<li><a href="dashboard.php"><i class="fa fa-home"></i> Home</a></li>
			<li><a href="products.php"><i class="fa fa-cubes"></i> Products</a></li>
			<li><a href="customers.php"><i class="fa fa-users"></i> Customers</a></li>
			<li><a href="orders.php"><i class="fa fa-random"></i> Orders</a></li>
			<li><a href="logout.php"><i class="fa fa-power-off"></i> Logout</a></li>
		</ul>
	</nav>
</div>
<div class="col-lg-12 thumbnail" style="width:103%">
	<?php 
		if (isset($_POST['add'])) {
			$name = $_POST['name'];
			$price = $_POST['price'];

			$query = mysqli_query($db, "INSERT INTO `products`(`product_id`, `name`, `price`) VALUES (NULL,'$name','$price')");

			if ($query) {
				success('Produt Added');
			}else{
				error('Fail to add product');
			}
		}

	 ?>
 <form action="" method="POST">
	<div class="col-lg-6">
		Product Name
		<input type="text" name="name" class="form-control">
	</div>
	<div class="col-lg-6">
		Product Price
		<input type="text" name="price" class="form-control">
	</div>
	<div class="col-lg-6">
		<br>
		<button type="submit" name="add" class="btn btn-success">Add Product</button>
	</div>
</form>
</div>
<div class="col-lg-12 thumbnail" style="width:103%">
	  <fieldset>
	  	   <legend><i class="fa fa-table"></i> Products</legend>
	  	  <table class="table table-striped">
	  	  	<thead>
	  	  		<tr>
	  	  			<th>S/N</th>
	  	  			<th>Product Name</th>
	  	  			<th>Price</th>
	  	  			<th></th>
	  	  		</tr>
	  	  	</thead>
	  	  	<tbody>
	  	  		<?php 
	  	  		$sn=0;
	  	  		$query = mysqli_query($db,"SELECT * FROM products");
	  	  		while ($row = mysqli_fetch_array($query)) {
	  	  			$sn++;
	  	  			echo "<tr>
	  	  				<td>".$sn."</td>
	  	  				<td>".$row['name']."</td>
	  	  				<td>".number_format($row['price'])."</td>
	  	  				<td class='btn-group'><a class='btn btn-primary btn-sm'><i class='fa fa-edit'></i></a><a class='btn btn-danger btn-sm'><i class='fa fa-trash-o'></i></a></td>
	  	  				</tr>";
	  	  		}
	  	  		 ?>
	  	  	</tbody>
	  	  </table>
	</fieldset>

</div>
</div>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
</body>
</html>
<html>