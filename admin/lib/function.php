<?php
  include_once "connection.php";
  //global $db;
  function success($message) {
  	   echo "<div class='alert alert-success'>$message</div>";
  }
  function error($message) {
      echo "<div class='alert alert-warning'>$message</div>";
  }

  function getRows($query) {
  	  global $db;
  	  $res = mysqli_query($db,$query);
  	  return mysqli_num_rows($res);
  }

  function fetchRows($query) {
  	  global $db;
  	  $res = mysqli_query($db,$query);
  	  return mysqli_fetch_array($res);
  }
?>